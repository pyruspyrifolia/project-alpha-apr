from django.urls import path
from projects.views import list_projects, show_projects, CreateProject


urlpatterns = [
    path("", list_projects, name="list_projects"),
    path("<int:id>", show_projects, name="show_project"),
    path("create/", CreateProject, name="create_project"),
]
