from django.shortcuts import render, redirect
from tasks.forms import TaskForm
from tasks.models import Task
from django.contrib.auth.decorators import login_required


# Create your views here.
@login_required
def CreateTask(request):
    if request.method == "POST":
        form = TaskForm(request.POST)
        if form.is_valid():
            form = form.save()
            return redirect("list_projects")
    else:
        form = TaskForm()

    context = {
        "form": form,
    }

    return render(request, "tasks/createtask.html", context)


@login_required
def list_tasks(request):
    task_list = Task.objects.filter(assignee=request.user)
    context = {"tasks_list": task_list}

    return render(request, "tasks/mytasks.html", context)
