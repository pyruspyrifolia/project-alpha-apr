from django.urls import path
from tasks.views import CreateTask, list_tasks

urlpatterns = [
    path("create/", CreateTask, name="create_task"),
    path("mine/", list_tasks, name="show_my_tasks"),
]
